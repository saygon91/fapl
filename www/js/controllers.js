angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $http, $ionicModal, $timeout) {

  $scope.url = 'http://fapl.0day.kz';

  $scope.load_table = function(table, season) {
    $scope.spinner = true;
    $http.get($scope.url+'/fapl/' + table, {params: {season: season}}).then(function(response) {
      $scope.table = response.data
      $scope.spinner = false;
    }, function(err) {
      console.error('ERR', err);
      $scope.spinner = false;
    });
  };

  $scope.load_fixtures_table = function(team) {
    $scope.spinner = true;
    $http.get($scope.url+'/fapl/fixtures', {params: {team: team}}).then(function(response) {
      $scope.table = response.data
      $scope.spinner = false;
    }, function(err) {
      console.error('ERR', err);
      $scope.spinner = false;
    });
  };

  // Create the login modal that we will use later
  // $ionicModal.fromTemplateUrl('templates/login.html', {
  //   scope: $scope
  // }).then(function(modal) {
  //   $scope.modal = modal;
  // });
})

.controller('NewsCtrl', function($scope, $http, $ionicLoading) {
  $scope.title = 'Главная'
  $scope.skip_count = 0;
  $scope.news = [];
  $scope.isMore = false;

  var get_news = function() {
    $ionicLoading.show({
      template: 'Идет загрузка...'
    });
    $http.get($scope.url+'/fapl/get_news', {params: {skip: 20*$scope.skip_count}}).success(function(response) {
      $scope.news = response.data.news;
      $scope.isMore = true;
      $ionicLoading.hide();
    }, function(err) {
      $ionicLoading.hide();
      $scope.error_ionic = err;
      console.error('ERR', err);
    })
  };

  get_news();

  $scope.doRefresh = function() {
    $scope.skip_count = 0;
    get_news();
    $scope.$broadcast('scroll.refreshComplete');
    $scope.$apply()
  };

  $scope.loadMore = function() {
    $scope.skip_count = $scope.skip_count + 1;
    $http.get($scope.url+'/fapl/get_news', {params: {skip: 20*$scope.skip_count}}).then(function(response) {
      if(response.data.news.length == 0)
        $scope.isMore = false;
      else
        $scope.news = $scope.news.concat(response.data.news);
      // Array.prototype.push.apply($scope.news, response.data.news);
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }, function(err) {
      console.error('ERR', err);
    })
  };
})

.controller('TagsCtrl', function($scope, $http, $stateParams) {
  $scope.title = 'Новости по тэгу: ' + $stateParams.tagName
  $scope.skip_count = 0;
  $scope.news = [];
  $scope.isMore = false;

  $scope.doRefresh = function() {
    $scope.skip_count = 0;
    get_news();
    $scope.$broadcast('scroll.refreshComplete');
    $scope.$apply()
  };

  var get_news = function() {
    $scope.spinner = true;
    $http.get($scope.url+'/fapl/get_news_by_tag', {params: {skip: 20*$scope.skip_count, tag: $stateParams.tagName}}).then(function(response) {
      $scope.news = response.data.news;
      $scope.isMore = true;
      $scope.spinner = false;
    }, function(err) {
      console.error('ERR', err);
      $scope.spinner = false;
    })
  };

  get_news();

  $scope.loadMore = function() {
    $scope.skip_count = $scope.skip_count + 1;
    $http.get($scope.url+'/fapl/get_news_by_tag', {params: {skip: 20*$scope.skip_count, tag: $stateParams.tagName}}).then(function(response) {
      if(response.data.news.length == 0)
        $scope.isMore = false;
      else
        $scope.news = $scope.news.concat(response.data.news);
      // Array.prototype.push.apply($scope.news, response.data.news);
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }, function(err) {
      console.error('ERR', err);
    })
  };
})

.controller('SingleNewsCtrl', function($scope, $http, $stateParams) {
  $http.get($scope.url+'/fapl/get_single_news', {params: {id: $stateParams.newsId}}).then(function(response) {
    $scope.current_news = response.data;
  }, function(err) {
    console.error('ERR', err);
  });
})

.controller('TableCtrl', function($scope) {
  $scope.title = "Таблица Премьер-Лиги";
  $scope.table_type = 'table';
  $scope.load_table($scope.table_type, 0);
})

.controller('TopScorersCtrl', function($scope) {
  $scope.title = "Бомбардиры Премьер-Лиги";
  $scope.table_type = 'top_scorers';
  $scope.load_table($scope.table_type, 0);
})

.controller('ResultsCtrl', function($scope) {
  $scope.title = "Результаты матчей Премьер-Лиги";
  $scope.table_type = 'results';
  $scope.load_table($scope.table_type, 0);
})

.controller('FixturesCtrl', function($scope, $stateParams) {
  $scope.title = "Расписание матчей Премьер-Лиги";
  $scope.table_type = 'fixtures';
  $scope.load_fixtures_table($stateParams.team);
})

.controller('BirthDaysCtrl', function($scope) {
  $scope.title = "Дни рождения игроков Премьер-Лиги";
  $scope.table_type = 'birth_days';
  $scope.load_table($scope.table_type, 0);
})

.filter('get_id', function() {
  return function(value) {
    var id;
    id = value.split('/');
    if(id[id.length-1] === "")
      return id[id.length-2];
    else
      return id[id.length-1];
  };
})

.filter('get_description', function() {
  return function(value) {
    var description;
    description = value.split('/>');
    return description[1];
  };
})

.filter('get_img', function() {
  return function(value) {
    var values;
    values = value.split('src="');
    values = values[1].split('"');
    return values[0];
  };
});
