// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.news', {
    url: "/news",
    views: {
      'menuContent': {
        templateUrl: "templates/news.html",
        controller: "NewsCtrl"
      }
    }
  })

  .state('app.tags', {
    url: "/tags/:tagName",
    views: {
      'menuContent': {
        templateUrl: "templates/news.html",
        controller: "TagsCtrl"
      }
    }
  })

  .state('app.table', {
    url: "/table",
    views: {
      'menuContent': {
        templateUrl: "templates/table.html",
        controller: "TableCtrl"
      }
    }
  })

  .state('app.top_scorers', {
    url: "/top_scorers",
    views: {
      'menuContent': {
        templateUrl: "templates/table.html",
        controller: "TopScorersCtrl"
      }
    }
  })

  .state('app.results', {
    url: "/results",
    views: {
      'menuContent': {
        templateUrl: "templates/table.html",
        controller: "ResultsCtrl"
      }
    }
  })

  .state('app.fixtures', {
    url: "/fixtures/:team",
    views: {
      'menuContent': {
        templateUrl: "templates/fixtures.html",
        controller: "FixturesCtrl"
      }
    }
  })

  .state('app.birth_days', {
    url: "/birth_days",
    views: {
      'menuContent': {
        templateUrl: "templates/table.html",
        controller: "BirthDaysCtrl"
      }
    }
  })

  .state('app.single', {
    url: "/posts/:newsId/",
    views: {
      'menuContent': {
        templateUrl: "templates/single_news.html",
        controller: 'SingleNewsCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/news');
});
